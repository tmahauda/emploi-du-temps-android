# Emploi du temps

<div align="center">
<img width="500" height="400" src="emploiDuTemps.jpg">
</div>

## Description du projet

Application mobile réalisée avec Android en MASTER INFO 1 à l'université d'Angers dans le cadre du module "Développement Mobile" durant l'année 2019-2020. \
Cette application consiste à afficher les créneaux de cours à venir pour les étudiants.

## Acteurs

### Réalisateurs

Ce projet a été réalisé par :
- Théo MAHAUDA : tmahauda@etud.univ-angers.fr.

### Encadrants

Ce projet fut encadré par une enseignant de l'unversité d'Angers :
- Vincent BARICHARD : vincent.barichard@univ-angers.fr.

## Organisation

Ce projet a été agit au sein de l'université d'Angers dans le cadre du module "Développement Mobile" du MASTER INFO 1.

## Date de réalisation

Ce projet a été éxécuté durant l'année 2019 sur la période du module pendant les heures de TP et personnelles à la maison. \
Il a été terminé et rendu le 20/12/2019.

## Technologies, outils et procédés utilisés

Ce projet a été accomplis avec les technologies, outils et procédés suivants :
- Java ;
- Android Studio ;
- Gradle ;
- Activites ;
- Cycle de vie ;
- SGBD SQLite ;
- Layouts ;
- ListView ;
- RecyclerView ;
- Adapter ;
- Menu ;
- Preferences.

## Objectifs

### Implémentation d'un RecylerView lié à une base de données

La première étape est de charger et afficher les contacts dans un RecyclerView. \
L'interface graphique est simple car composée uniquement d'un RecyclerView. \
Une base de données SQLite a été utilisée pour accéder aux données. Il a été nécessaire d'implémenter le contrat de la base avec toutes les classes nécessaires. 

Le résultat obtenu :
<div align="center">
<img width="200" height="400" src="Affichage_Creneau.png">
</div>

### Suppression d'un élément par ajout d'un "Swipe"

Dans cette partie, il fallait mettre en place la suppression d'un élément de la liste par le geste de balayage (swipe). De plus lorsque l'appareil effectue une rotation, les éléments supprimés restent supprimés !

### Ajout d'un nouveau créneau

Dans cette partie, il fallait implémenter un menu qui lance une nouvelle activité permetant de saisir un nouveau créneau. Ce nouveau créneau sera intégré avec les données existantes (dans la base de données) puis le RecyclerView sera rafraîchi en conséquence :
<div align="center">
<img width="200" height="400" src="Ajout_Creneau.png">
</div>