package fr.univangers.m1info.emploidutemps.data.database;

import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import androidx.appcompat.app.AppCompatActivity;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

public class DBHelper extends SQLiteOpenHelper {

    /**
     * Tag qui permet de logger la classe
     */
    private static final String TAG = DBHelper.class.getSimpleName();

    // Si le schéma de la base change, il faut incrémenter cette valeur
    private static final int DATABASE_VERSION = 2;
    private static final String DATABASE_NAME = "emploiDuTemps.db";

    //Liste des tables renseignés, représentées par des contrats
    private List<DBContrat<?>> contrats;

    /**
     * Constructeur qui initialise la base de données
     * @param context
     * @param contrats
     */
    public DBHelper(AppCompatActivity context, DBContrat<?>... contrats) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.contrats = Arrays.asList(contrats);
    }

    /**
     * On crée toutes les tables
     * @param db
     */
    @Override
    public void onCreate(SQLiteDatabase db) {
        for(DBContrat<?> contrat : this.contrats) {
            db.execSQL(this.getSQLCreateEntries(contrat));
        }
    }

    /**
     * Récupère une ligne de commande pour créer la BD (table + colonnes) à partir d'un contrat
     * @param contrat
     * @return la ligne de commande
     */
    private String getSQLCreateEntries(DBContrat<?> contrat) {
        StringBuilder sb = new StringBuilder();

        //On ajoute le nom de la table
        sb.append("CREATE TABLE IF NOT EXISTS ");
        sb.append(contrat.getTable());
        sb.append(" (");

        //Puis les définitions de chaque colonne
        for(ColumnObject columnObject : contrat.getColumns()) {
            sb.append(columnObject.getNameColumn());
            sb.append(" ");
            sb.append(columnObject.getTypeColumn());
            sb.append(",");
        }

        //On enlève la dernière virgule
        sb.deleteCharAt(sb.length() - 1);
        sb.append(")");

        return sb.toString();
    }

    /**
     * On supprime toutes les tables puis on recrée la base lors de la migration
     * @param db
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        for(DBContrat<?> contrat : this.contrats) {
            db.execSQL(this.getSQLDeleteEntries(contrat));
        }
        this.onCreate(db);
    }

    /**
     * Récupère une ligne de commande pour supprimer une table à partir d'un contrat
     * @param contrat
     * @return la ligne de commande
     */
    private String getSQLDeleteEntries(DBContrat<?> contrat) {
        StringBuilder sb = new StringBuilder();

        sb.append("DROP TABLE IF EXISTS ");
        sb.append(contrat.getTable());

        return sb.toString();
    }

    /**
     * Mettre à jour la base de données
     * @param db
     * @param oldVersion
     * @param newVersion
     */
    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        this.onUpgrade(db, oldVersion, newVersion);
    }

    public List<DBContrat<?>> getContrats() {
        return contrats;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DBHelper dbHelper = (DBHelper) o;
        return Objects.equals(contrats, dbHelper.contrats);
    }

    @Override
    public int hashCode() {
        return Objects.hash(contrats);
    }

    @Override
    public String toString() {
        return "DBHelper{" +
                "contrats=" + contrats +
                '}';
    }
}
