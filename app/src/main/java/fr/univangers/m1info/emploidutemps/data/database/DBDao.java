package fr.univangers.m1info.emploidutemps.data.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import fr.univangers.m1info.emploidutemps.data.Datable;
import fr.univangers.m1info.emploidutemps.model.Model;

public class DBDao<T extends Model> implements Datable<T> {

    /**
     * Tag qui permet de logger la classe
     */
    private static final String TAG = DBDao.class.getName();

    private Context context;
    private DBHelper dbHelper;
    private DBContrat<T> dbContrat;
    private Cursor cursor;
    private SQLiteDatabase db;

    public DBDao(Context context, DBHelper dbHelper, DBContrat<T> dbContrat) {
        this.context = context;
        this.dbHelper = dbHelper;
        this.dbContrat = dbContrat;
    }

    public boolean openAsWritable() {
        Log.i(TAG, "Ouverture de la base de données en mode lecture/écriture");
        this.db = this.dbHelper.getWritableDatabase();
        return this.open();
    }

    public boolean openAsReadable() {
        Log.i(TAG, "Ouverture de la base de données en lecture seule");
        this.db = this.dbHelper.getReadableDatabase();
        return this.open();
    }

    @Override
    public boolean open() {
        this.swapCursor();
        return true;
    }

    @Override
    public boolean close() {
        Log.i(TAG, "Fermeture de la base de données");
        if(this.cursor != null)
            this.cursor.close();
        if(this.db != null)
            this.db.close();
        if(this.dbHelper != null)
            this.dbHelper.close();
        return true;
    }

    /**
     * Récupère un curseur sur tous les enregistrement de la table donnée
     */
    private void swapCursor() {
        Log.i(TAG, "Récupération du curseur");

        // Projection utilisée pour la requête
        String[] projection = this.dbContrat.getProjectionColumns();

        //Trie des résultats
        String sort = null;
        if(this.dbContrat.getColumnSortAsc() != null)
            sort = this.dbContrat.getColumnSortAsc().getNameColumn() + " ASC";
        if(this.dbContrat.getColumnSortDesc() != null)
            sort = this.dbContrat.getColumnSortDesc().getNameColumn() + " DESC";

        //On récupère le curseur
        this.cursor = db.query(
                this.dbContrat.getTable(), // La table
                projection, // Les colonnes à récupérer (null pour toutes)
                null, // La clause WHERE
                null, // Les valeurs pour la clause WHERE
                null, // Grouper les lignes
                null, // Filtrer les lignes
                sort // Trier les lignes
        );
    }

    /**
     * Récupére les valeurs d'un objet en clé-valeur pour l'enregistrer dans la BD
     * @param model le model convertit en clé-valeur
     * @return les valeurs de l'objet
     */
    private ContentValues getValues(T model) {
        Log.i(TAG, "Récupération des données du model");
        Log.i(TAG, "Model : " + model);

        //Création d'un ContentValues (fonctionne comme une HashMap)
        ContentValues values = new ContentValues();

        //On enregistre les données de chaque colonne à partir du modele
        for(ColumnObject columnObject : this.dbContrat.getColumns()) {
            ContentValues valuesModel = columnObject.getValue(model);
            Log.i(TAG, "Valeurs : " + values);
            if(valuesModel != null)
                values.putAll(valuesModel);
        }

        return values;
    }

    /**
     * Méthode qui permet de créer un objet par réflection et modifie les champs de l'objet
     * à partir du curseur courant
     * Attention l'objet doit fournir un contructeur sans argument !
     * @return
     */
    private T createFromCurrentCursor() {
        Log.i(TAG, "Création du model à partir de la position courante du curseur");

        try {
            T model = this.dbContrat.getContrat().newInstance();

            //On enregistre les données de chaque colonne dans le modele
            for(ColumnObject columnObject : this.dbContrat.getColumns()) {
                columnObject.setValue(model, this.cursor);
            }

            Log.i(TAG, "Nouvelle instance : " + model);

            return model;
        } catch (IllegalAccessException e) {
            Log.e(TAG, e.getMessage(), e);
            return null;
        } catch (InstantiationException e) {
            Log.e(TAG, e.getMessage(), e);
            return null;
        }
    }

    @Override
    public boolean create(T model) {
        Log.i(TAG, "Insertion d'un model dans la BD : " + model);

        ContentValues values = this.getValues(model);
        //L'identifiant est auto-incrémenté
        values.remove(this.dbContrat.getColumnIdentifiant().getNameColumn());

        db.beginTransaction();

        try {
            db.insert(this.dbContrat.getTable(), null, values);
            db.setTransactionSuccessful();
            this.swapCursor();
            return true;
        }
        catch (SQLException e) {
            Log.e(TAG, e.getMessage(), e);
            return false;
        }
        finally {
            db.endTransaction();
        }
    }

    @Override
    public List<T> read() {
        Log.i(TAG, "Lecture des models dans une collection");

        List<T> models = new ArrayList<>();
        this.cursor.moveToPosition(-1);

        while(this.cursor.moveToNext()) {
            T model = createFromCurrentCursor();
            Log.i(TAG, "Model : " + model);
            models.add(model);
        }

        return models;
    }

    @Override
    public T read(int position) {
        Log.i(TAG, "Lecture d'un model à la position " + position);
        this.cursor.moveToPosition(position);
        return createFromCurrentCursor();
    }

    @Override
    public boolean update(T model) {
        Log.i(TAG, "Mise à jour du model " + model);

        ContentValues values = this.getValues(model);
        //L'identifiant est auto-incrémenté
        values.remove(this.dbContrat.getColumnIdentifiant().getNameColumn());

        this.db.beginTransaction();

        try {
            this.db.update(this.dbContrat.getTable(), values, this.dbContrat.getColumnIdentifiant().getNameColumn()
                    + " = ?" , new String[] {String.valueOf(model.getIdentifiant())});
            this.db.setTransactionSuccessful();
            this.swapCursor();
            return true;
        }
        catch (SQLException e) {
            Log.e(TAG, e.getMessage(), e);
            return false;
        }
        finally {
            this.db.endTransaction();
        }
    }

    @Override
    public boolean delete() {
        Log.i(TAG, "Suppression de tous les models");

        this.db.beginTransaction();

        try {
            db.delete(this.dbContrat.getTable(), null, null);
            db.setTransactionSuccessful();
            this.swapCursor();
            return true;
        }
        catch (SQLException e) {
            Log.e(TAG, e.getMessage(), e);
            return false;
        }
        finally {
            db.endTransaction();
        }
    }

    @Override
    public boolean delete(T model) {
        Log.i(TAG, "Suppression du model " + model);

        //this.cursor.moveToPosition(position);
        //T oldModel = this.createFromCurrentCursor();
        //Log.i(TAG, "Model : " + oldModel);

        this.db.beginTransaction();

        try {
            this.db.delete(this.dbContrat.getTable(), this.dbContrat.getColumnIdentifiant().getNameColumn()
                    + " = ?" , new String[]{String.valueOf(model.getIdentifiant())});
            this.db.setTransactionSuccessful();
            this.swapCursor();
            return true;
        }
        catch (SQLException e) {
            Log.e(TAG, e.getMessage(), e);
            return false;
        }
        finally {
            this.db.endTransaction();
        }
    }

    @Override
    public int size() {
        return this.cursor.getCount();
    }

    @Override
    public int position(T model) {
       return this.read().indexOf(model);
    }

    @Override
    public void populateData(List<T> models) {
        for(T model : models) {
            this.create(model);
        }
    }

    public Context getContext() {
        return context;
    }

    public DBHelper getDbHelper() {
        return dbHelper;
    }

    public DBContrat<T> getDbContrat() {
        return dbContrat;
    }

    public Cursor getCursor() {
        return cursor;
    }

    public SQLiteDatabase getDb() {
        return db;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DBDao<?> dbDao = (DBDao<?>) o;
        return Objects.equals(context, dbDao.context) &&
                Objects.equals(dbHelper, dbDao.dbHelper) &&
                Objects.equals(dbContrat, dbDao.dbContrat) &&
                Objects.equals(cursor, dbDao.cursor) &&
                Objects.equals(db, dbDao.db);
    }

    @Override
    public int hashCode() {
        return Objects.hash(context, dbHelper, dbContrat, cursor, db);
    }

    @Override
    public String toString() {
        return "DBDao{" +
                "context=" + context +
                ", dbHelper=" + dbHelper +
                ", dbContrat=" + dbContrat +
                ", cursor=" + cursor +
                ", db=" + db +
                '}';
    }
}
