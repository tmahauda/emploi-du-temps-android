package fr.univangers.m1info.emploidutemps.model;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.Objects;
import fr.univangers.m1info.emploidutemps.data.database.Column;
import fr.univangers.m1info.emploidutemps.data.database.Table;

@Table(name = "EmploiDuTemps")
public class EmploiDuTemps extends Model implements Parcelable {

    @Column(name = "date", isSortByAsc = true)
    private String date;

    @Column(name = "personne")
    private String personne;

    @Column(name = "matiere")
    private String matiere;

    public EmploiDuTemps() {

    }

    public EmploiDuTemps(String date, String personne, String matiere) {
        this.date = date;
        this.personne = personne;
        this.matiere = matiere;
    }

    protected EmploiDuTemps(Parcel in) {
        this.identifiant = in.readInt();
        this.position = in.readInt();
        this.date = in.readString();
        this.personne = in.readString();
        this.matiere = in.readString();
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getPersonne() {
        return personne;
    }

    public void setPersonne(String personne) {
        this.personne = personne;
    }

    public String getMatiere() {
        return matiere;
    }

    public void setMatiere(String matiere) {
        this.matiere = matiere;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        EmploiDuTemps that = (EmploiDuTemps) o;
        return Objects.equals(date, that.date) &&
                Objects.equals(personne, that.personne) &&
                Objects.equals(matiere, that.matiere);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), date, personne, matiere);
    }

    @Override
    public String toString() {
        return "EmploiDuTemps{" +
                "date='" + date + '\'' +
                ", personne='" + personne + '\'' +
                ", matiere='" + matiere + '\'' +
                ", position=" + position +
                ", identifiant=" + identifiant +
                '}';
    }

    //Parcelable = permettre de sérialiser/désérialiser un objet en flux d'octets pour transférer d'une activité à une autre

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.identifiant);
        parcel.writeInt(this.position);
        parcel.writeString(this.date);
        parcel.writeString(this.personne);
        parcel.writeString(this.matiere);
    }

    public static final Creator<EmploiDuTemps> CREATOR = new Creator<EmploiDuTemps>() {
        @Override
        public EmploiDuTemps createFromParcel(Parcel in) {
            return new EmploiDuTemps(in);
        }

        @Override
        public EmploiDuTemps[] newArray(int size) {
            return new EmploiDuTemps[size];
        }
    };
}
