package fr.univangers.m1info.emploidutemps.adapter.recyclerview;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;

import fr.univangers.m1info.emploidutemps.R;

public class ViewHolderEDT extends RecyclerView.ViewHolder {

    /**
     * Tag qui permet de logger la classe
     */
    private static final String TAG = ViewHolderEDT.class.getSimpleName();

    protected TextView dateEDT;
    protected TextView personneEDT;
    protected TextView matiereEDT;

    public ViewHolderEDT(View view) {
        super(view);
        this.dateEDT = view.findViewById(R.id.textViewDateEDT);
        this.personneEDT = view.findViewById(R.id.textViewPersonneEDT);
        this.matiereEDT = view.findViewById(R.id.textViewMatiereEDT);
    }
}
