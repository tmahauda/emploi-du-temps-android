package fr.univangers.m1info.emploidutemps.adapter;


import fr.univangers.m1info.emploidutemps.data.Datable;
import fr.univangers.m1info.emploidutemps.model.Model;

public interface Adaptable<T extends Model> extends Datable<T> {

}
