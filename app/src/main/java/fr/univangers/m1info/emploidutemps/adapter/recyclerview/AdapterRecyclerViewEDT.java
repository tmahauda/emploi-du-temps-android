package fr.univangers.m1info.emploidutemps.adapter.recyclerview;

import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.appcompat.app.AppCompatActivity;

import fr.univangers.m1info.emploidutemps.R;
import fr.univangers.m1info.emploidutemps.activity.EDTActivity;
import fr.univangers.m1info.emploidutemps.activity.MainActivity;
import fr.univangers.m1info.emploidutemps.data.Datable;
import fr.univangers.m1info.emploidutemps.model.EmploiDuTemps;

public class AdapterRecyclerViewEDT extends AdapterRecyclerView<EmploiDuTemps, ViewHolderEDT> {

    /**
     * Tag qui permet de logger la classe
     */
    private static final String TAG = AdapterRecyclerViewEDT.class.getSimpleName();

    public AdapterRecyclerViewEDT(AppCompatActivity context, Datable<EmploiDuTemps> datable) {
        super(context, datable);
    }

    @Override
    public ViewHolderEDT onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(this.context).inflate(R.layout.row_edt, parent, false);
        return new ViewHolderEDT(view);
    }

    @Override
    public void onBindViewHolder(ViewHolderEDT holder, int position) {
        final EmploiDuTemps edt = this.read(position);
        edt.setPosition(position);

        //On enregistre un écouteur pour démarrer une nouvelle activité
        //afin de modifier l'item
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.i(TAG, "Modification d'un edt");
                Intent edtActivity = new Intent(context, EDTActivity.class);
                edtActivity.putExtra(EDTActivity.UPDATE_EDT, edt);
                context.startActivityForResult(edtActivity, MainActivity.UPDATE_EDT_ID);
            }
        });

        //On stocke la tache dans le tag pour les swipes
        holder.itemView.setTag(edt);

        holder.dateEDT.setText(edt.getDate());
        holder.personneEDT.setText(edt.getPersonne());
        holder.matiereEDT.setText(edt.getMatiere());
    }
}
