package fr.univangers.m1info.emploidutemps.data;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import fr.univangers.m1info.emploidutemps.model.EmploiDuTemps;
import fr.univangers.m1info.emploidutemps.util.UtilEDT;

public final class FakeData {

    public static List<EmploiDuTemps> getListEDT() {
        List<EmploiDuTemps> edt = new ArrayList<>();

        edt.add(new EmploiDuTemps(UtilEDT.getFormatDate(new Date()), "B. Da Mota", "Réseau"));
        edt.add(new EmploiDuTemps(UtilEDT.getFormatDate(new Date()), "A. Todoskoff", "Projet"));
        edt.add(new EmploiDuTemps(UtilEDT.getFormatDate(new Date()), "O. Goudet", "Design Patterns"));
        edt.add(new EmploiDuTemps(UtilEDT.getFormatDate(new Date()), "J-M Chrantrein", "Docker"));

        return edt;
    }
}
