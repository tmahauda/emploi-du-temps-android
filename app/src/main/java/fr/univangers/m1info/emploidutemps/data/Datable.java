package fr.univangers.m1info.emploidutemps.data;

import java.util.List;

import fr.univangers.m1info.emploidutemps.model.Model;

/**
 * Interface qui offre à tout datable (listView ou recylerView) la posibilité de :
 * - Récupérer un model par sa position dans la liste
 * - Mettre à jour un model par sa position dans la liste
 * - Ajouter un model
 * - Supprimer un model par sa position dans la liste
 * - Récupérer la taille de la liste
 * @param <T> le model manipulé
 */
public interface Datable<T extends Model>  {

    /**
     * Enregistrer un model
     */
    public boolean create(T model);

    /**
     * Récupère tous les models enregistrés
     * @return
     */
    public List<T> read();

    /**
     * Récupère un model par son ID
     * @param id
     * @return
     */
    public T read(int id);

    /**
     * Mettre à jour un model par son ID
     * @param id
     * @param model
     */
    public boolean update(T model);

    /**
     * Supprime tous les models
     * @return
     */
    public boolean delete();

    /**
     * Supprimer un model par son ID
     * @param id
     */
    public boolean delete(T model);

    /**
     * Récupérer la taille
     * @return la taille
     */
    public int size();

    /**
     * Récupère la position d'un model
     * @param model
     * @return la position
     */
    public int position(T model);

    /**
     * Peupler la source de données avec des données
     * @param models
     */
    public void populateData(List<T> models);

    /**
     * Ouvrir la source de données
     * @return
     */
    public boolean open();

    /**
     * Fermer le source de données
     * @return
     */
    public boolean close();
}
