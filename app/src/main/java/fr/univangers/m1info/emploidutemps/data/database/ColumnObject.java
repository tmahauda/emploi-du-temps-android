package fr.univangers.m1info.emploidutemps.data.database;

import android.content.ContentValues;
import android.database.Cursor;
import java.lang.reflect.Field;
import java.util.Objects;

public class ColumnObject {

    /**
     * Tag qui permet de logger la classe
     */
    private static final String TAG = ColumnObject.class.getSimpleName();

    private Field field;
    private Column annotationColumn;
    private String nameColumn;
    private String typeColumn;

    public ColumnObject(Field field) {
        this.field = field;
        this.annotationColumn = field.getAnnotation(Column.class);
        this.initNameColumn();
        this.initTypeColumn();
    }

    /**
     * Récupère le nom d'un champ à partir d'un attribut de classe
     * @return
     */
    private void initNameColumn() {
        //Si le nom de la colonne n'est pas renseigné alors on retourne le nom du champ
        // Sinon on retourne le nom déclaré dans l'annotation
        if(this.annotationColumn.name().equals("")) this.nameColumn = field.getName();
        else this.nameColumn = annotationColumn.name();
    }

    /**
     * Récupère le nom d'un type à partir d'un attribut de classe
     * @return
     */
    private void initTypeColumn() {
        Class<?> typeField = this.field.getType();

        if(typeField.isAssignableFrom(int.class) || typeField.isAssignableFrom(Integer.class))
            this.typeColumn = "INTEGER";
        else if(typeField.isAssignableFrom(String.class))
            this.typeColumn = "TEXT";

        //Si c'est une clé primaire
        if(this.annotationColumn.isPrimaryKey())
            this.typeColumn += " PRIMARY KEY";

        //Si c'est AUTOINCREMENT
        if(this.annotationColumn.isAutoIncrement())
            this.typeColumn += " AUTOINCREMENT";
    }

    /**
     * Récupère toutes les valeurs d'une instance d'un objet
     * @param instance
     * @return
     */
    public ContentValues getValue(Object instance) {
        if(instance == null) return null;

        ContentValues contentValues = new ContentValues();
        Class<?> typeField = this.field.getType();

        this.field.setAccessible(true);
        try {
            if(typeField.isAssignableFrom(int.class) || typeField.isAssignableFrom(Integer.class))
                contentValues.put(this.nameColumn, this.field.getInt(instance));
            else if(typeField.isAssignableFrom(String.class))
                contentValues.put(this.nameColumn, (String)this.field.get(instance));
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        this.field.setAccessible(false);

        return contentValues;
    }

    /**
     * Ajoute des données à partir de la position courrante d'un curseur dans une instance d'un objet
     * @param instance
     * @param cursor
     */
    public void setValue(Object instance, Cursor cursor) {
        Class<?> typeField = this.field.getType();

        this.field.setAccessible(true);
        try {
            if(typeField.isAssignableFrom(int.class) || typeField.isAssignableFrom(Integer.class))
                this.field.setInt(instance, cursor.getInt(cursor.getColumnIndexOrThrow(this.nameColumn)));
            else if(typeField.isAssignableFrom(String.class))
                this.field.set(instance, cursor.getString(cursor.getColumnIndexOrThrow(this.nameColumn)));
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        this.field.setAccessible(false);
    }

    public Field getField() {
        return field;
    }

    public Column getAnnotationColumn() {
        return annotationColumn;
    }

    public String getNameColumn() {
        return nameColumn;
    }

    public String getTypeColumn() {
        return typeColumn;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ColumnObject that = (ColumnObject) o;
        return Objects.equals(field, that.field) &&
                Objects.equals(annotationColumn, that.annotationColumn) &&
                Objects.equals(nameColumn, that.nameColumn) &&
                Objects.equals(typeColumn, that.typeColumn);
    }

    @Override
    public int hashCode() {
        return Objects.hash(field, annotationColumn, nameColumn, typeColumn);
    }

    @Override
    public String toString() {
        return "ColumnObject{" +
                "field=" + field +
                ", annotationColumn=" + annotationColumn +
                ", nameColumn='" + nameColumn + '\'' +
                ", typeColumn='" + typeColumn + '\'' +
                '}';
    }
}
