package fr.univangers.m1info.emploidutemps.util;

import java.text.SimpleDateFormat;
import java.util.Date;

public class UtilEDT {

    private UtilEDT() {}

    public static String getFormatDate(Date date) {
        String format = "dd/MM/yyyy HH:mm";
        SimpleDateFormat formater = new SimpleDateFormat(format);
        return formater.format(date);
    }

}
