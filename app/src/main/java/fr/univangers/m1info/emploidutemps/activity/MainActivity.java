package fr.univangers.m1info.emploidutemps.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import java.util.ArrayList;

import fr.univangers.m1info.emploidutemps.R;
import fr.univangers.m1info.emploidutemps.adapter.Adaptable;
import fr.univangers.m1info.emploidutemps.adapter.FactoryAdapter;
import fr.univangers.m1info.emploidutemps.data.Datable;
import fr.univangers.m1info.emploidutemps.data.FactoryData;
import fr.univangers.m1info.emploidutemps.model.EmploiDuTemps;

public class MainActivity extends AppCompatActivity {

    /**
     * Tag qui permet de logger la classe
     */
    private static final String TAG = MainActivity.class.getSimpleName();

    /**
     * Etiquette qui permet de récupérer la liste lors de la rotation de l'écran
     */
    private static final String EDT = "EDT";

    /**
     * ID qui permet de vérifier si l'activite "ajouter une nouveau créneau" est bien
     * l'activité retourné dans onActivityResult
     */
    public static final int ADD_EDT_ID = 1;

    /**
     * ID qui permet de vérifier si l'activite "mise à jour un nouveau créneau" est bien
     * l'activité retourné dans onActivityResult
     */
    public static final int UPDATE_EDT_ID = 2;

    /**
     * La source de données
     */
    private Datable<EmploiDuTemps> dataEDT;

    /**
     * Adapter avec RC
     */
    private Adaptable<EmploiDuTemps> adapterEDT;

    /**
     * RecylerView
     */
    private RecyclerView recyclerViewEDT;

    @Override
    protected void onDestroy() {
        this.adapterEDT.close();
        super.onDestroy();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //On choisit comme source de données la BD
        this.dataEDT = FactoryData.buildDataEDTFromBD(this);

        //On choisir un RecyclerView pour afficher les données
        this.recyclerViewEDT = findViewById(R.id.recyclerViewEDT);
        this.adapterEDT = FactoryAdapter.buildAdapterReclyclerViewEDT(this, this.recyclerViewEDT, this.dataEDT);
    }

    /**
     * On gonfle le menu par un nouveau menu afin de créer de nouveau cours
     * @param menu
     * @return
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        Log.i(TAG, "Nouveau menu");
        getMenuInflater().inflate(R.menu.main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    /**
     * Lors de l'ajout d'un nouveau cours on execute une nouvelle activité
     * @param item
     * @return
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_add_edt:
                Log.i(TAG, "Création d'une nouveau cours");
                Intent addTache = new Intent(this, EDTActivity.class);
                this.startActivityForResult(addTache, ADD_EDT_ID);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * On traite les activités lancés depuis cette activité
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        String message = "";

        if (requestCode == ADD_EDT_ID && resultCode == RESULT_OK) {
            EmploiDuTemps edt = data.getParcelableExtra(EDTActivity.ADD_EDT);

            if(this.adapterEDT.create(edt)) {
                message = "Cours " + edt.getMatiere() + " ajoutée";
                Log.i(TAG, message);
                Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
            } else {
                message = "Cours " + edt.getMatiere() + " non ajoutée";
                Log.i(TAG, message);
                Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
            }
        } else if(requestCode == UPDATE_EDT_ID && resultCode == RESULT_OK) {
            EmploiDuTemps edt = data.getParcelableExtra(EDTActivity.UPDATE_EDT);

            if(this.adapterEDT.update(edt)) {
                message = "Cours " + edt.getMatiere() + " mise à jour";
                Log.i(TAG, message);
                Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
            } else {
                message = "Cours " + edt.getMatiere() + " non mise à jour";
                Log.i(TAG, message);
                Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
            }
        }
    }

    /**
     * On sauvegarde la structure de données dans le cas d'un éventuel pivotement de l'écran
     * @param outState
     */
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelableArrayList(EDT, new ArrayList<>(this.adapterEDT.read()));
    }
}
