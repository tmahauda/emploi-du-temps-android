package fr.univangers.m1info.emploidutemps.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import fr.univangers.m1info.emploidutemps.R;
import fr.univangers.m1info.emploidutemps.model.EmploiDuTemps;

public class EDTActivity extends AppCompatActivity {

    public static final String ADD_EDT = "AddEDT";
    public static final String UPDATE_EDT = "UpdateEDT";

    private EditText editTextNewDate;
    private EditText editTextNewPersonne;
    private EditText editTextNewMatiere;

    private Button buttonOK;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edt);

        this.editTextNewDate = findViewById(R.id.editTextNewDateEDT);
        this.editTextNewPersonne = findViewById(R.id.editTextNewPersonneEDT);
        this.editTextNewMatiere = findViewById(R.id.editTextNewMatiereEDT);
        this.buttonOK = findViewById(R.id.buttonOK);

        //Dans le cas d'une modification
        final EmploiDuTemps edt = this.getIntent().getParcelableExtra(UPDATE_EDT);
        if(edt != null) {
            this.editTextNewDate.setText(edt.getDate());
            this.editTextNewPersonne.setText(edt.getPersonne());
            this.editTextNewMatiere.setText(edt.getMatiere());
        }

        this.buttonOK.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Intent newEDT = new Intent();
                if(edt != null) {
                    edt.setDate(editTextNewDate.getText().toString());
                    edt.setPersonne(editTextNewPersonne.getText().toString());
                    edt.setMatiere(editTextNewMatiere.getText().toString());
                    newEDT.putExtra(UPDATE_EDT, edt);
                } else {
                    EmploiDuTemps cours = new EmploiDuTemps(editTextNewDate.getText().toString(), editTextNewPersonne.getText().toString(), editTextNewMatiere.getText().toString());
                    newEDT.putExtra(ADD_EDT, cours);
                }
                setResult(RESULT_OK, newEDT);
                finish();
            }
        });
    }
}
