package fr.univangers.m1info.emploidutemps.data;

import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;

import java.util.List;

import fr.univangers.m1info.emploidutemps.data.database.DBContrat;
import fr.univangers.m1info.emploidutemps.data.database.DBDao;
import fr.univangers.m1info.emploidutemps.data.database.DBHelper;
import fr.univangers.m1info.emploidutemps.model.EmploiDuTemps;

public class FactoryData {

    private static final String TAG = FactoryData.class.getName();

    private FactoryData() {

    }

    public static Datable<EmploiDuTemps> buildDataEDTFromBD(AppCompatActivity context) {
        Log.i(TAG, "Initialisation de la base de données");

        DBContrat<EmploiDuTemps> dbContratEDT = new DBContrat<>(EmploiDuTemps.class);
        Log.i(TAG, "Le contrat : " + dbContratEDT);

        DBHelper dbHelperEDT = new DBHelper(context, dbContratEDT);
        Log.i(TAG, "La base de données : " + dbHelperEDT);

        DBDao<EmploiDuTemps> dbDaoEDT = new DBDao<>(context, dbHelperEDT, dbContratEDT);
        Log.i(TAG, "La DAO : " + dbDaoEDT);

        dbDaoEDT.openAsWritable();
        Log.i(TAG, "Ouverture de la BD en mode écriture");

        //List<EmploiDuTemps> edts = FakeData.getListEDT();
        //dbDaoEDT.delete();
        //dbDaoEDT.populateData(edts);
        //Log.i(TAG, "Peuplement de la BD avec des données fictives : " + edts);

        return dbDaoEDT;
    }
}
