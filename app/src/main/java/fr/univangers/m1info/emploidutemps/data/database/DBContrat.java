package fr.univangers.m1info.emploidutemps.data.database;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import fr.univangers.m1info.emploidutemps.model.Model;

/**
 * Contrat qui représente toutes les caractéristiques d'une table à partir des méta-données d'une classe
 * en exploitant les mécanismes de la réflection
 * @param <T> le modèle de classe
 */
public class DBContrat<T extends Model> {

    /**
     * Tag qui permet de logger la classe
     */
    private static final String TAG = DBContrat.class.getSimpleName();

    /**
     * Le contrat représenté par les méta-données de la classe
     */
    private Class<T> contrat;

    /**
     * Le nom de la table
     */
    private String table;

    /**
     * L'identifiant de la table
     */
    private ColumnObject columnIdentifiant;

    /**
     * La colonne utilisé pour le trie ascendant
     */
    private ColumnObject columnSortAsc;

    /**
     * La colonne utilisé pour le trie déscendant
     */
    private ColumnObject columnSortDesc;

    /**
     * Toutes les colonnes de la table
     */
    private List<ColumnObject> columns;

    public DBContrat(Class<T> contrat) {
        this.contrat = contrat;
        this.initTable();
        this.initColumns();
    }

    /**
     * Récupère le nom de la table à partir de l'annotation @Table
     * @return
     */
    private void initTable() {
        //Si le contrat possède le nom de la table
        if(this.contrat.isAnnotationPresent(Table.class)) {
           Table table = this.contrat.getAnnotation(Table.class);
           //Si la table n'est pas renseigné on retourne le nom de la classe
            // Sinon on retourne le nom déclaré dans l'annotation
           if(table == null) this.table = this.contrat.getSimpleName();
           else {
               if(table.name().equals("")) this.table = this.contrat.getSimpleName();
               else this.table = table.name();
           }
        } else this.table = this.contrat.getSimpleName();
    }

    /**
     * Récupère une map avec :
     * clé = nom de la colonne
     * valeur = type de la colonne
     * @return
     */
    private void initColumns() {
        this.columns = new ArrayList<>();

        for(Field field : this.getAllDeclaredField(new ArrayList<Field>(), this.contrat)) {
            //Si l'annotation column n'est pas présente on passe au champ suivant
            if(!field.isAnnotationPresent(Column.class)) continue;

            Column column = field.getAnnotation(Column.class);
            if(column == null) continue;

            ColumnObject columnObject = new ColumnObject(field);
            this.columns.add(columnObject);

            //Si la colonne est une clé primaire
            if(column.isPrimaryKey())
                this.columnIdentifiant = columnObject;

            if(column.isSortByAsc())
                this.columnSortAsc = columnObject;

            if(column.isSortByDesc())
                this.columnSortDesc = columnObject;
        }
    }

    /**
     * Méthode qui permet de récupérer tous les champs
     * @return
     */
    private List<Field> getAllDeclaredField(List<Field> fields, Class<?> type) {
        fields.addAll(Arrays.asList(type.getDeclaredFields()));

        if(type.getSuperclass() != null) {
            this.getAllDeclaredField(fields, type.getSuperclass());
        }

        return fields;
    }

    /**
     * Récupère une projection de tous les champs
     * @return
     */
    public String[] getProjectionColumns() {
        String[] projections = new String[this.columns.size()];

        for(int i=0; i<this.columns.size(); i++) {
            projections[i] = this.columns.get(i).getNameColumn();
        }

        return projections;
    }

    public Class<T> getContrat() {
        return contrat;
    }

    public String getTable() {
        return table;
    }

    public ColumnObject getColumnIdentifiant() {
        return columnIdentifiant;
    }

    public ColumnObject getColumnSortAsc() {
        return columnSortAsc;
    }

    public ColumnObject getColumnSortDesc() {
        return columnSortDesc;
    }

    public List<ColumnObject> getColumns() {
        return columns;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DBContrat<?> dbContrat = (DBContrat<?>) o;
        return Objects.equals(contrat, dbContrat.contrat) &&
                Objects.equals(table, dbContrat.table) &&
                Objects.equals(columnIdentifiant, dbContrat.columnIdentifiant) &&
                Objects.equals(columns, dbContrat.columns);
    }

    @Override
    public int hashCode() {
        return Objects.hash(contrat, table, columnIdentifiant, columns);
    }

    @Override
    public String toString() {
        return "DBContrat{" +
                "contrat=" + contrat +
                ", table='" + table + '\'' +
                ", columnIdentifiant=" + columnIdentifiant +
                ", columns=" + columns +
                '}';
    }
}
