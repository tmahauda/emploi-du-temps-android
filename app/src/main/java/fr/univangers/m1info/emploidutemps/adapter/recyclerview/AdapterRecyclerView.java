package fr.univangers.m1info.emploidutemps.adapter.recyclerview;

import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;
import java.util.List;

import fr.univangers.m1info.emploidutemps.adapter.Adaptable;
import fr.univangers.m1info.emploidutemps.data.Datable;
import fr.univangers.m1info.emploidutemps.model.Model;

public abstract class AdapterRecyclerView<T extends Model, K extends RecyclerView.ViewHolder> extends RecyclerView.Adapter<K> implements Adaptable<T> {

    /**
     * Tag qui permet de logger la classe
     */
    private static final String TAG = AdapterRecyclerView.class.getSimpleName();

    /**
     * Stratégies : accès par une liste ou un curseur ou toute autre structure de données qui implémente Datable
     */
    protected Datable<T> datable;

    /**
     * Contexte de l'application
     */
    protected AppCompatActivity context;

    public AdapterRecyclerView(AppCompatActivity context, Datable<T> datable) {
        this.context = context;
        this.datable = datable;
    }

    @Override
    public boolean create(T model) {
        Log.i(TAG, "Création du model " + model);
        boolean create = this.datable.create(model);
        //Attention ! il faut notifier à la vue qu'un item à été ajouté
        this.notifyDataSetChanged();
        return create;
    }

    @Override
    public List<T> read() {
        return this.datable.read();
    }

    @Override
    public T read(int position) {
        return this.datable.read(position);
    }

    @Override
    public boolean update(T model) {
        Log.i(TAG, "Mise à jour du model " + model);
        boolean update = this.datable.update(model);
        //Attention ! il faut notifier à la vue que l'item à été modifié
        this.notifyItemChanged(model.getPosition());
        //Attention ! l'élément peut changer de position, il faut le notifier à la vue
        this.notifyItemMoved(model.getPosition(), this.position(model));
        return update;
    }

    @Override
    public boolean delete() {
        Log.i(TAG, "Suppression de tous les models");
        boolean delete = this.datable.delete();
        //Pour une suppression total on notifie toute la vue !
        this.notifyDataSetChanged();
        return delete;
    }

    @Override
    public boolean delete(T model) {
        Log.i(TAG, "Suppression du model " + model);
        boolean delete = this.datable.delete(model);
        //Attention ! il faut notifier à la vue que l'item à été supprimé
        this.notifyItemRemoved(model.getPosition());
        //Attention ! Il faut réagancer les éléments pour réaligner
        this.notifyItemRangeChanged(model.getPosition(), this.getItemCount() - model.getPosition());
        return delete;
    }

    @Override
    public int size() {
        return this.datable.size();
    }

    @Override
    public int position(T model) {
        return this.datable.position(model);
    }

    @Override
    public void populateData(List<T> models) {
        this.datable.populateData(models);
    }

    @Override
    public boolean open() {
        return this.datable.open();
    }

    @Override
    public boolean close() {
        return this.datable.close();
    }

    @Override
    public int getItemCount() {
        return this.size();
    }

    @Override
    public long getItemId(int position) {
        return this.read(position).getIdentifiant();
    }
}
