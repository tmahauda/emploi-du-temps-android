package fr.univangers.m1info.emploidutemps.adapter;

import android.util.Log;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import fr.univangers.m1info.emploidutemps.adapter.recyclerview.AdapterRecyclerViewEDT;
import fr.univangers.m1info.emploidutemps.data.Datable;
import fr.univangers.m1info.emploidutemps.model.EmploiDuTemps;

public class FactoryAdapter {

    /**
     * Tag qui permet de logger la classe
     */
    private static final String TAG = FactoryAdapter.class.getSimpleName();

    private FactoryAdapter() {

    }

    @SuppressWarnings("unchecked")
    public static Adaptable<EmploiDuTemps> buildAdapterReclyclerViewEDT(AppCompatActivity context, RecyclerView rv, Datable<EmploiDuTemps> data) {
        // Choix du LayoutManager du RecyclerView, ici un LinearLayout
        // qui mesurera et rangera les items
        rv.setLayoutManager(new LinearLayoutManager(context));

        //Création de l'adapter et liaison avec le RecyclerView
        RecyclerView.Adapter<?> adapterRecyclerViewEDT = new AdapterRecyclerViewEDT(context, data);
        rv.setAdapter(adapterRecyclerViewEDT);
        Adaptable<EmploiDuTemps> adapter = (Adaptable<EmploiDuTemps>)adapterRecyclerViewEDT;

        //Liaison avec un itemTouchHelper
        attachRecyclerViewToItemTouchHelper(context, rv, adapter);

        return adapter;
    }

    private static void attachRecyclerViewToItemTouchHelper(final AppCompatActivity context, final RecyclerView rv, final Adaptable<EmploiDuTemps> adapter) {
        //Donner la possibilité de supprimer des items dans le RV
        ItemTouchHelper.SimpleCallback itemTouch = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.RIGHT | ItemTouchHelper.LEFT) {

            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                EmploiDuTemps edt = (EmploiDuTemps) viewHolder.itemView.getTag();
                adapter.delete(edt);
                Log.i(TAG, "Créneau " + edt.getMatiere() + " supprimée");
                Toast.makeText(context, "Créneau " + edt.getMatiere() + " supprimée", Toast.LENGTH_SHORT).show();
            }
        };

        new ItemTouchHelper(itemTouch).attachToRecyclerView(rv);
    }
}
