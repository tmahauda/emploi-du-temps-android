package fr.univangers.m1info.emploidutemps.data.database;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Column {

    String name() default "";
    boolean isPrimaryKey() default false;
    boolean isAutoIncrement() default false;
    boolean isSortByAsc() default false;
    boolean isSortByDesc() default false;
}