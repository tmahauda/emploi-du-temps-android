package fr.univangers.m1info.emploidutemps.model;

import java.util.Objects;

import fr.univangers.m1info.emploidutemps.data.database.Column;

public abstract class Model {

    protected int position;

    @Column(name = "identifiant", isPrimaryKey = true, isAutoIncrement = true)
    protected int identifiant;

    public Model() {

    }

    public Model(int identifiant) {
        this.identifiant = identifiant;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public int getIdentifiant() {
        return identifiant;
    }

    public void setIdentifiant(int identifiant) {
        this.identifiant = identifiant;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Model tache = (Model) o;
        return identifiant == tache.identifiant;
    }

    @Override
    public int hashCode() {
        return Objects.hash(identifiant);
    }

    @Override
    public String toString() {
        return "Model{" +
                "position=" + position +
                ", identifiant=" + identifiant +
                '}';
    }
}
